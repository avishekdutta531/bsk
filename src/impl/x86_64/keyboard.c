#include "keyboard.h"
#include "print.h"
#include <stdbool.h>

#define KEYBOARD_PORT 0x60

static char key_buffer[256];
static int buffer_position = 0;

const char scancode_to_char[] = {
    0, 0, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b',
    '\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',
    0, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', 0,
    '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 0, '*', 0, ' ', 0
};

void keyboard_handler() {
    uint8_t scancode = inb(KEYBOARD_PORT);
    
    if (scancode & 0x80) return; // Ignore key releases
    
    char c = scancode_to_char[scancode];
    
    if (c == '\b' && buffer_position > 0) {
        buffer_position--;
        print_char('\b');
    } else if (c != '\b' && buffer_position < sizeof(key_buffer) - 1) {
        key_buffer[buffer_position++] = c;
        print_char(c);
    }
}

char* get_input() {
    key_buffer[buffer_position] = '\0';
    buffer_position = 0;
    return key_buffer;
}

uint8_t inb(uint16_t port) {
    uint8_t ret;
    asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
} 